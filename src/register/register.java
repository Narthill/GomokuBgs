package register;

import dao.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by ChenHao-PC on 2017/6/8.
 */
@WebServlet(name = "register")
public class register extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("html;charset=UTF-8");
        PrintWriter out=response.getWriter();

        String username=request.getParameter("username");
        String password=request.getParameter("password");

        UserDao dao=new UserDao();

        if(dao.selectSameName(username)==0){//检查是否有同名
            int i=dao.register(username,password);
            if(i>0){
                out.print("ok");
            }else{
                out.print("no");
            }
        }else {
            out.print("sameName");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
