package conn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {//数据库连接类
	// 定义变量
	public static String userName = "root";
	public static String usetPass = "root";
	public static String url = "jdbc:mysql://localhost:3306/gomoku";

	public static Connection getConnection() {
		Connection con = null;

		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(url, userName, usetPass);
			return con;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void main(String[] args) {
		System.out.println(DBConnection.getConnection()+"yes");
	}
}