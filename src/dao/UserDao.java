package dao;
import java.sql.*;
import java.util.*;
import Userinfo.UserInfo;
import conn.DBConnection;
/**
 * Created by ChenHao-PC on 2017/5/22.
 */
public class UserDao {
    public UserInfo login(String username,String password){
        try {
            Connection con = DBConnection.getConnection();
            String sql = "select * from usr where username =? AND password=?";// Sql注入 'OR '1'='1
            PreparedStatement pre = con.prepareStatement(sql);//预编译
			pre.setString(1, username);//赋值占位符
            pre.setString(2, password);
            ResultSet res = pre.executeQuery();//获取查询结果
            if(res.next()){
                //记录指针向下移动一个位置，如果其指向一条有效记录，则返回真；否则返回假

                UserInfo user = new UserInfo();
                user.setId(res.getInt("id"));
                user.setUsername(res.getString("username"));
                user.setPassword(res.getString("password"));
                user.setPower(res.getString("power"));
                user.setCount(res.getInt("count"));
                user.setWinCount(res.getInt("winCount"));
                user.setLoseCount(res.getInt("loseCount"));

                pre.close();
                res.close();
                con.close();

                return user;
            }else{
                pre.close();
                res.close();
                con.close();
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    public int selectSameName(String username){
        try {
            Connection con = DBConnection.getConnection();
            String sql = "select * from usr where username =?";
            PreparedStatement pre = con.prepareStatement(sql);//预编译
            pre.setString(1, username);//赋值占位符
            ResultSet res = pre.executeQuery();//获取查询结果
            if(res.next()){//如果没有查到那么next方法会返回false，下面就不会再执行，它决不会返回null
                return 1;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return 0;
    }

    public int register(String username,String password){
        try {
            Connection con = DBConnection.getConnection();
            String sql = "INSERT INTO usr (username,password,power) VALUES (?,?,'user')";
            PreparedStatement pre = con.prepareStatement(sql);//预编译
            pre.setString(1, username);//赋值占位符
            pre.setString(2, password);
            int i = pre.executeUpdate();//更新结果统计
            if(i>0){
                pre.close();
                con.close();
                return i;
            }else{
                pre.close();
                con.close();
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return 0;
    }
//    public static void main(String[] args){
//        UserDao dao=new UserDao();
//        UserInfo user=dao.login("管理员");
//        System.out.println(user.toString());
//        int i=dao.register("蛤蟆","1494494daomao");
//        System.out.println(i);
//        System.out.println(dao.selectSameName("12131"));
//    }
}

