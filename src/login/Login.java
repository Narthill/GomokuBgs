package login;

import Userinfo.UserInfo;
import dao.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by ChenHao-PC on 2017/5/22.
 */
@WebServlet(name = "Servlet")
    public class Login extends HttpServlet {
        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            this.doPost(req, resp);
        }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("html;charset=UTF-8");
        PrintWriter out=response.getWriter();

        String username=request.getParameter("username");
        String password=request.getParameter("password");

        if((null!=username)&&(null!=password)){//如果输入不为空
            UserInfo user;
            UserDao dao=new UserDao();
            user=dao.login(username,password);

            if(user!=null){//如果查询不为空
                if(username.equals(user.getUsername()) && password.equals(user.getPassword())){//前后台输入比较
                    if(user.getPower().equals("admin")){//判断权限
                        out.print("admin");
                    }else{
                        System.out.println(username+","+password);
                        out.print("true");
                    }
                }else {
                    System.out.println(username+","+password);
                    out.print("false");
                }
            }else{
                System.out.println(username+","+password);
                out.print("false");
            }

        }else {
            out.print("null");
        }
        out.close();
    }
}
