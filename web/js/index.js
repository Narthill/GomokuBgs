var username=document.getElementById("username");
var password=document.getElementById("password");
var userValue;
var passValue;
document.getElementById("login_btn").onclick=function () {
    login();
};

function judgeNull() {
    userValue=username.value;
    passValue=password.value;
    var sign=true;

        if(userValue===""){
            username.setAttribute("placeholder","用户名不能为空");
            username.setAttribute("class","login_input login_input_change");//多个class用空格
            sign=false;
        }

        if(passValue===""){
            password.setAttribute("placeholder","密码不能为空");
            password.setAttribute("class","login_input login_input_change");
            sign=false;
        }

    if(sign){
        return true;
    }else{
        return false;
    }
}



function login(){

    if(judgeNull()){

        var xmlhttp;
        if (window.XMLHttpRequest) {
            //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
            xmlhttp=new XMLHttpRequest();
        } else {
            // IE6, IE5 浏览器执行代码
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.open("post","Login",true);

        xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded;charset=UTF-8");

        xmlhttp.send("username="+userValue+"&password="+passValue);

        xmlhttp.onreadystatechange=function() {

            if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {
                if(xmlhttp.responseText==="admin"){
                    window.location.href="backstage.html";
                }else if(xmlhttp.responseText==="true"){
                    window.location.href="success.html";
                }else if(xmlhttp.responseText==="false"){
                    document.getElementById('get').innerHTML="账号密码输入有误";
                }else if(xmlhttp.responseText==="null"){
                    document.getElementById('get').innerHTML="账号密码输入为空";
                }
            }
        }
    }
}


