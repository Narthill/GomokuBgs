var username=document.getElementById("reg_username");
var password=document.getElementById("reg_password");
var confirm=document.getElementById("confirm");
var userValue;
var passValue;
document.getElementById("register_btn").onclick=function () {
    register();
};

function judge() {
    userValue=username.value;
    passValue=password.value;

    var sign=true;

        if(userValue===""){
            username.setAttribute("placeholder","用户名不能为空");
            username.setAttribute("class","register_input register_input_change");//多个class用空格
            sign=false;
        }else{
            username.setAttribute("placeholder","用户名");
            username.setAttribute("class","register_input");
        }

        if(passValue===""){
            password.setAttribute("placeholder","密码不能为空");
            password.setAttribute("class","register_input register_input_change");
            sign=false;
        }else{
            password.setAttribute("placeholder","密码");
            password.setAttribute("class","register_input");
        }

        if(confirm.value===""){
            confirm.setAttribute("placeholder","确认密码不能为空");
            confirm.setAttribute("class","register_input register_input_change");
            sign=false;
        }else if(confirm.value!=passValue && passValue!=""){
            confirm.value="";
            confirm.setAttribute("placeholder","两次输入不一致");
            confirm.setAttribute("class","register_input register_input_change");
            sign=false;
        }else{
            confirm.setAttribute("placeholder","确认密码");
            confirm.setAttribute("class","register_input");
        }

    if(sign){
        return true;
    }else{
        return false;
    }
}

function register(){

    if(judge()){

        var xmlhttp;
        if (window.XMLHttpRequest) {
            //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
            xmlhttp=new XMLHttpRequest();
        } else {
            // IE6, IE5 浏览器执行代码
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.open("post","register",true);//post方法传输

        xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded;charset=UTF-8");//设置请求头相关信息

        xmlhttp.send("username="+userValue+"&password="+passValue);//传输报文主体参数

        xmlhttp.onreadystatechange=function() {//onreadystatechange负责存放处理服务器响应的函数

            if (xmlhttp.readyState==4 && xmlhttp.status==200)//readyState 属性存有服务器响应的状态信息
            {
                if(xmlhttp.responseText==="ok"){
                    window.location.href="success.html";
                }else if(xmlhttp.responseText==="sameName"){
                    alert("此用户名已被占用");
                }else{
                    alert("注册失败");
                }

            }
        }
    }
}


